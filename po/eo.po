# Translation of Pamac.
# Copyright (C) 2013-2019 Manjaro Developers <manjaro-dev@manjaro.org>
# This file is distributed under the same license as the Pamac package.
# Guillaume Benoit <guillaume@manjaro.org>, 2013-2019.
# 
# Translators:
# Anderson Tavares <nocturne.pe@gmail.com>, 2013
# Anderson Tavares <nocturne.pe@gmail.com>, 2013
# juanda097 <juanda097@protonmail.ch>, 2015
# juanda097 <juanda097@protonmail.ch>, 2015
# Kurt Ankh Phoenix <kurtphoenix@tuta.io>, 2018
# Kurt Ankh Phoenix <kurtphoenix@tuta.io>, 2018
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pamac\n"
"Report-Msgid-Bugs-To: guillaume@manjaro.org\n"
"POT-Creation-Date: 2019-08-06 16:57+0200\n"
"PO-Revision-Date: 2019-08-07 00:47+0000\n"
"Last-Translator: philm <philm@manjaro.org>\n"
"Language-Team: Esperanto (http://www.transifex.com/manjarolinux/manjaro-pamac/language/eo/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: eo\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/polkit/org.manjaro.pamac.policy.in
msgid "Authentication is required"
msgstr "Aŭtentikigo estas bezonata"

#: src/alpm_utils.vala src/database.vala
msgid "Failed to initialize alpm library"
msgstr "Malsukcesis inici alpm bibliotekon"

#: src/alpm_utils.vala
msgid "Failed to synchronize any databases"
msgstr ""

#: src/alpm_utils.vala
msgid "Failed to init transaction"
msgstr ""

#: src/alpm_utils.vala src/transaction.vala src/transaction-gtk.vala
msgid "Failed to prepare transaction"
msgstr ""

#: src/alpm_utils.vala src/cli.vala
#, c-format
msgid "target not found: %s"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "package %s does not have a valid architecture"
msgstr "pakaĵo %s ne havas validan arkitekturon"

#: src/alpm_utils.vala
#, c-format
msgid "unable to satisfy dependency '%s' required by %s"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "installing %s (%s) breaks dependency '%s' required by %s"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "removing %s breaks dependency '%s' required by %s"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "%s and %s are in conflict"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "%s needs to be removed but it is a locked package"
msgstr "%sbezonas foriĝi sed ĝi estas ŝlosita pakaĵo"

#: src/alpm_utils.vala
msgid "Failed to commit transaction"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "%s exists in both %s and %s"
msgstr "%sekzistas en ambaŭ %s kaj %s"

#: src/alpm_utils.vala
#, c-format
msgid "%s: %s already exists in filesystem"
msgstr "%s: %s jam ekzistas en dosiersistemo"

#: src/alpm_utils.vala
#, c-format
msgid "%s is invalid or corrupted"
msgstr "%s estas malvalida aŭ difektita"

#: src/alpm_utils.vala
#, c-format
msgid "%s appears to be truncated: %jd/%jd bytes\n"
msgstr ""

#: src/alpm_utils.vala
#, c-format
msgid "failed retrieving file '%s' from %s : %s\n"
msgstr ""

#: src/system_daemon.vala
msgid "Authentication failed"
msgstr "Aŭtentikigo malsukcesis"

#: src/database.vala src/transaction-gtk.vala src/manager_window.vala
#: src/transaction-cli.vala src/cli.vala resources/preferences_dialog.ui
msgid "AUR"
msgstr ""

#: src/database.vala src/manager_window.vala
msgid "Explicitly installed"
msgstr "Eksplicite instalita"

#: src/database.vala src/manager_window.vala
msgid "Installed as a dependency for another package"
msgstr "Instalita kiel dependeco por alia pakaĵo"

#: src/database.vala
msgid "Unknown"
msgstr "Nekonata"

#: src/database.vala
msgid "Yes"
msgstr "Jes"

#: src/database.vala
msgid "No"
msgstr "Ne"

#: src/transaction.vala
msgid "Refreshing mirrors list"
msgstr ""

#: src/transaction.vala
msgid "Preparing"
msgstr "Preparanta"

#: src/transaction.vala
msgid "Synchronizing package databases"
msgstr ""

#: src/transaction.vala
msgid "Starting full system upgrade"
msgstr ""

#: src/transaction.vala src/cli.vala
#, c-format
msgid "Cloning %s build files"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Generating %s informations"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Checking %s dependencies"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "key %s could not be imported"
msgstr ""

#: src/transaction.vala
msgid "Waiting for another package manager to quit"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Building %s"
msgstr "Kunmetanta %s"

#: src/transaction.vala
msgid "Transaction cancelled"
msgstr ""

#: src/transaction.vala
msgid "Checking dependencies"
msgstr "Kontrolanta Dependecojn"

#: src/transaction.vala
msgid "Checking file conflicts"
msgstr "Kontrolanta dosierajn konfliktojn"

#: src/transaction.vala
msgid "Resolving dependencies"
msgstr "Solvanta Dependecojn"

#: src/transaction.vala
msgid "Checking inter-conflicts"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Installing %s"
msgstr "Instalanta %s"

#: src/transaction.vala
#, c-format
msgid "Upgrading %s"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Reinstalling %s"
msgstr "Reinstalanta %s"

#: src/transaction.vala
#, c-format
msgid "Downgrading %s"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Removing %s"
msgstr ""

#: src/transaction.vala
msgid "Checking integrity"
msgstr "Kontrolanta integrecon"

#: src/transaction.vala
msgid "Loading packages files"
msgstr "Ŝarĝanta pakaĵajn dosierojn"

#: src/transaction.vala
msgid "Checking delta integrity"
msgstr ""

#: src/transaction.vala
msgid "Applying deltas"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Generating %s with %s"
msgstr ""

#: src/transaction.vala
msgid "Generation succeeded"
msgstr ""

#: src/transaction.vala
msgid "Generation failed"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Configuring %s"
msgstr ""

#: src/transaction.vala
msgid "Checking available disk space"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "%s optionally requires %s"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Database file for %s does not exist"
msgstr ""

#: src/transaction.vala
msgid "Checking keyring"
msgstr ""

#: src/transaction.vala
msgid "Downloading required keys"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "%s installed as %s.pacnew"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "%s installed as %s.pacsave"
msgstr ""

#: src/transaction.vala
msgid "Running pre-transaction hooks"
msgstr ""

#: src/transaction.vala
msgid "Running post-transaction hooks"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "Downloading %s"
msgstr "Deŝut %s"

#: src/transaction.vala
#, c-format
msgid "About %u seconds remaining"
msgstr ""

#: src/transaction.vala
#, c-format
msgid "About %lu minute remaining"
msgid_plural "About %lu minutes remaining"
msgstr[0] ""
msgstr[1] ""

#: src/transaction.vala
#, c-format
msgid "Refreshing %s"
msgstr ""

#: src/transaction.vala src/transaction-cli.vala src/cli.vala
msgid "Error"
msgstr "Eraro"

#: src/transaction.vala src/transaction-gtk.vala
msgid "Warning"
msgstr ""

#: src/transaction.vala src/installer.vala src/cli.vala
msgid "Nothing to do"
msgstr "Nenio por fari"

#: src/transaction.vala src/transaction-gtk.vala
msgid "Transaction successfully finished"
msgstr "Finita sukcese transakcio"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Choose optional dependencies for %s"
msgstr ""

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Choose a provider for %s"
msgstr ""

#: src/transaction-gtk.vala
msgid "Import PGP key"
msgstr ""

#: src/transaction-gtk.vala
msgid "Trust and Import"
msgstr ""

#: src/transaction-gtk.vala src/manager_window.vala
#: resources/transaction_sum_dialog.ui resources/manager_window.ui
#: resources/choose_pkgs_dialog.ui
msgid "_Cancel"
msgstr "_Nulig"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "The PGP key %s is needed to verify %s source files"
msgstr ""

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Trust %s and import the PGP key"
msgstr ""

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To remove"
msgstr "Forigi"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To downgrade"
msgstr "Reveni"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To build"
msgstr "Kunmeti"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To install"
msgstr "Instali"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To reinstall"
msgstr "Reinstali"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To upgrade"
msgstr ""

#: src/transaction-gtk.vala src/manager_window.vala src/transaction-cli.vala
msgid "Total download size"
msgstr ""

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Edit %s build files"
msgstr ""

#: src/transaction-gtk.vala
msgid "Save"
msgstr ""

#: src/transaction-gtk.vala resources/progress_dialog.ui
#: resources/history_dialog.ui resources/preferences_dialog.ui
msgid "_Close"
msgstr "_Ferm"

#: src/transaction-gtk.vala src/tray.vala
msgid "Package Manager"
msgstr "Pakaĵo Administranto"

#: src/installer.vala src/cli.vala
msgid "Install packages from repositories, path or url"
msgstr ""

#: src/installer.vala src/cli.vala
msgid "Remove packages"
msgstr ""

#: src/installer.vala src/cli.vala
msgid "package(s)"
msgstr ""

#: src/tray.vala src/manager_window.vala src/cli.vala
msgid "Your system is up-to-date"
msgstr "Via sistemo estas ĝisdatigata"

#: src/tray.vala
msgid "_Quit"
msgstr ""

#: src/tray.vala src/cli.vala
#, c-format
msgid "%u available update"
msgid_plural "%u available updates"
msgstr[0] ""
msgstr[1] ""

#: src/tray.vala src/manager_window.vala resources/progress_dialog.ui
#: resources/manager_window.ui
msgid "Details"
msgstr "Detaloj"

#: src/manager_window.vala
msgid "No package found"
msgstr ""

#: src/manager_window.vala
msgid "Checking for Updates"
msgstr ""

#: src/manager_window.vala
#, c-format
msgid "%u pending operation"
msgid_plural "%u pending operations"
msgstr[0] ""
msgstr[1] ""

#: src/manager_window.vala
msgid "Categories"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Groups"
msgstr "Grupoj"

#: src/manager_window.vala
msgid "Repositories"
msgstr ""

#: src/manager_window.vala src/cli.vala resources/manager_window.ui
msgid "Installed"
msgstr "Instalita"

#: src/manager_window.vala
msgid "Orphans"
msgstr "Orfoj"

#: src/manager_window.vala
msgid "Foreign"
msgstr ""

#: src/manager_window.vala
msgid "Accessories"
msgstr ""

#: src/manager_window.vala
msgid "Audio & Video"
msgstr ""

#: src/manager_window.vala
msgid "Development"
msgstr ""

#: src/manager_window.vala
msgid "Education"
msgstr ""

#: src/manager_window.vala
msgid "Games"
msgstr ""

#: src/manager_window.vala
msgid "Graphics"
msgstr ""

#: src/manager_window.vala
msgid "Internet"
msgstr ""

#: src/manager_window.vala
msgid "Office"
msgstr ""

#: src/manager_window.vala
msgid "Science"
msgstr ""

#: src/manager_window.vala
msgid "Settings"
msgstr ""

#: src/manager_window.vala
msgid "System Tools"
msgstr ""

#: src/manager_window.vala
msgid "Dependencies"
msgstr ""

#: src/manager_window.vala
msgid "Files"
msgstr "Dosieroj"

#: src/manager_window.vala
msgid "Build files"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Install Reason"
msgstr "Instalo Kialo"

#: src/manager_window.vala
msgid "Mark as explicitly installed"
msgstr ""

#: src/manager_window.vala resources/manager_window.ui
msgid "Install"
msgstr "Instali"

#: src/manager_window.vala src/cli.vala
msgid "Licenses"
msgstr "Permesiloj"

#: src/manager_window.vala src/cli.vala resources/manager_window.ui
msgid "Repository"
msgstr "Deponejo"

#: src/manager_window.vala src/cli.vala
msgid "Package Base"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Maintainer"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "First Submitted"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Last Modified"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Votes"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Out of Date"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Packager"
msgstr "Pakaĵkreanto"

#: src/manager_window.vala src/cli.vala
msgid "Build Date"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Install Date"
msgstr "Instalo Dato"

#: src/manager_window.vala src/cli.vala
msgid "Signatures"
msgstr "Subskriboj"

#: src/manager_window.vala src/cli.vala
msgid "Backup files"
msgstr "Kopisekurecaj Dosieroj"

#: src/manager_window.vala src/cli.vala
msgid "Depends On"
msgstr "Dependas De"

#: src/manager_window.vala src/cli.vala
msgid "Optional Dependencies"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Required By"
msgstr "Postulata De"

#: src/manager_window.vala src/cli.vala
msgid "Optional For"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Provides"
msgstr "Provizas"

#: src/manager_window.vala src/cli.vala
msgid "Replaces"
msgstr "Anstataŭigas"

#: src/manager_window.vala src/cli.vala
msgid "Conflicts With"
msgstr "Konfliktas Kun"

#: src/manager_window.vala src/cli.vala
msgid "Make Dependencies"
msgstr ""

#: src/manager_window.vala src/cli.vala
msgid "Check Dependencies"
msgstr ""

#: src/manager_window.vala
msgid "Upgrade"
msgstr ""

#: src/manager_window.vala resources/manager_window.ui
msgid "Remove"
msgstr "Forigi"

#: src/manager_window.vala src/cli.vala resources/manager_window.ui
msgid "Size"
msgstr "Grando"

#: src/manager_window.vala resources/manager_window.ui
msgid "Install Local Packages"
msgstr ""

#: src/manager_window.vala
msgid "_Open"
msgstr "_Open"

#: src/manager_window.vala
msgid "Alpm Package"
msgstr ""

#: src/manager_window.vala
msgid "A Gtk3 frontend for libalpm"
msgstr ""

#: src/preferences_dialog.vala resources/preferences_dialog.ui
msgid "How often to check for updates, value in hours"
msgstr ""

#: src/preferences_dialog.vala resources/preferences_dialog.ui
msgid "Maximum parallel downloads"
msgstr ""

#: src/preferences_dialog.vala src/cli.vala resources/preferences_dialog.ui
msgid "Number of versions of each package to keep in the cache"
msgstr ""

#: src/preferences_dialog.vala
msgid "Build directory"
msgstr ""

#: src/preferences_dialog.vala
msgid "Worldwide"
msgstr ""

#: src/preferences_dialog.vala src/cli.vala
msgid "To delete"
msgstr ""

#: src/preferences_dialog.vala src/cli.vala
#, c-format
msgid "%u file"
msgid_plural "%u files"
msgstr[0] ""
msgstr[1] ""

#: src/preferences_dialog.vala
msgid "Choose Ignored Upgrades"
msgstr ""

#: src/transaction-cli.vala src/cli.vala
#, c-format
msgid "Enter a selection (default=%s)"
msgstr ""

#: src/transaction-cli.vala
msgid "none"
msgstr ""

#: src/transaction-cli.vala
#, c-format
msgid "Enter a number (default=%d)"
msgstr ""

#: src/transaction-cli.vala src/cli.vala
msgid "[y/N]"
msgstr ""

#: src/transaction-cli.vala src/cli.vala
msgid "y"
msgstr ""

#: src/transaction-cli.vala src/cli.vala
msgid "yes"
msgstr ""

#: src/transaction-cli.vala resources/transaction_sum_dialog.ui
msgid "Edit build files"
msgstr ""

#: src/transaction-cli.vala
msgid "Total installed size"
msgstr ""

#: src/transaction-cli.vala
msgid "Total removed size"
msgstr ""

#: src/transaction-cli.vala
msgid "Apply transaction"
msgstr ""

#: src/transaction-cli.vala
#, c-format
msgid "View %s build files diff"
msgstr ""

#: src/cli.vala
msgid "Building packages as root is not allowed"
msgstr ""

#: src/cli.vala
msgid "No PKGBUILD file found in current directory"
msgstr ""

#: src/cli.vala
msgid "Check development packages updates as root is not allowed"
msgstr ""

#: src/cli.vala
msgid "Available actions"
msgstr ""

#: src/cli.vala
msgid "action"
msgstr ""

#: src/cli.vala
msgid "options"
msgstr ""

#: src/cli.vala
msgid "Search for packages or files, multiple search terms can be specified"
msgstr ""

#: src/cli.vala
msgid "file(s)"
msgstr ""

#: src/cli.vala
msgid "also search in AUR"
msgstr ""

#: src/cli.vala
msgid ""
"search for packages which own the given filenames (filenames can be partial)"
msgstr ""

#: src/cli.vala
msgid "Display package details, multiple packages can be specified"
msgstr ""

#: src/cli.vala
msgid "List packages, groups, repositories or files"
msgstr ""

#: src/cli.vala
msgid "group(s)"
msgstr ""

#: src/cli.vala
msgid "repo(s)"
msgstr ""

#: src/cli.vala
msgid "list installed packages"
msgstr ""

#: src/cli.vala
msgid ""
"list packages that were installed as dependencies but are no longer required"
" by any installed package"
msgstr ""

#: src/cli.vala
msgid "list packages that were not found in the repositories"
msgstr ""

#: src/cli.vala
msgid ""
"list all packages that are members of the given groups, if no group is given"
" list all groups"
msgstr ""

#: src/cli.vala
msgid ""
"list all packages available in the given repos, if no repo is given list all"
" repos"
msgstr ""

#: src/cli.vala
msgid "list files owned by the given packages"
msgstr ""

#: src/cli.vala
msgid "Clone or sync packages build files from AUR"
msgstr ""

#: src/cli.vala
msgid "dir"
msgstr ""

#: src/cli.vala
msgid ""
"build directory, if no directory is given the one specified in pamac.conf "
"file is used"
msgstr ""

#: src/cli.vala
msgid "also clone needed dependencies"
msgstr ""

#: src/cli.vala
msgid "overwrite existing files"
msgstr ""

#: src/cli.vala
msgid "Build packages from AUR and install them with their dependencies"
msgstr ""

#: src/cli.vala
msgid ""
"If no package name is given, use the PKGBUILD file in the current directory"
msgstr ""

#: src/cli.vala
msgid ""
"The build directory will be the parent directory, --builddir option will be "
"ignored"
msgstr ""

#: src/cli.vala
msgid "do not clone build files from AUR, only use local files"
msgstr ""

#: src/cli.vala
msgid "bypass any and all confirmation messages"
msgstr ""

#: src/cli.vala
msgid "glob"
msgstr ""

#: src/cli.vala
msgid ""
"ignore a package upgrade, multiple packages can be specified by separating "
"them with a comma"
msgstr ""

#: src/cli.vala
msgid ""
"overwrite conflicting files, multiple patterns can be specified by "
"separating them with a comma"
msgstr ""

#: src/cli.vala
msgid "Reinstall packages"
msgstr ""

#: src/cli.vala
msgid ""
"remove dependencies that are not required by other packages, if this option "
"is used without package name remove all orphans"
msgstr ""

#: src/cli.vala
msgid "Safely check for updates without modifiying the databases"
msgstr ""

#: src/cli.vala
msgid "Exit code is 100 if updates are available"
msgstr ""

#: src/cli.vala
msgid "also check updates in AUR"
msgstr ""

#: src/cli.vala
msgid "also check development packages updates (use with --aur)"
msgstr ""

#: src/cli.vala
msgid "only print one line per update"
msgstr ""

#: src/cli.vala
msgid ""
"build directory (use with --devel), if no directory is given the one "
"specified in pamac.conf file is used"
msgstr ""

#: src/cli.vala
msgid "Upgrade your system"
msgstr ""

#: src/cli.vala
msgid "also upgrade packages installed from AUR"
msgstr ""

#: src/cli.vala
msgid "also upgrade development packages (use with --aur)"
msgstr ""

#: src/cli.vala
msgid ""
"build directory (use with --aur), if no directory is given the one specified"
" in pamac.conf file is used"
msgstr ""

#: src/cli.vala
msgid "force the refresh of the databases"
msgstr ""

#: src/cli.vala
msgid "enable package downgrades"
msgstr ""

#: src/cli.vala
msgid "Clean packages cache or build files"
msgstr ""

#: src/cli.vala
msgid "number"
msgstr ""

#: src/cli.vala
msgid ""
"specify how many versions of each package are kept in the cache directory"
msgstr ""

#: src/cli.vala
msgid "only target uninstalled packages"
msgstr ""

#: src/cli.vala
msgid ""
"remove all build files, the build directory is the one specified in "
"pamac.conf"
msgstr ""

#: src/cli.vala
msgid "do not remove files, only find candidate packages"
msgstr ""

#: src/cli.vala
msgid "also display all files names"
msgstr ""

#: src/cli.vala resources/manager_window.ui
msgid "Name"
msgstr "Nomo"

#: src/cli.vala
msgid "Version"
msgstr "Versio"

#: src/cli.vala
msgid "Description"
msgstr ""

#: src/cli.vala
msgid "URL"
msgstr ""

#: src/cli.vala
#, c-format
msgid "No package owns %s"
msgstr ""

#: src/cli.vala
#, c-format
msgid "%s is owned by %s"
msgstr ""

#: src/cli.vala resources/preferences_dialog.ui
msgid "Remove only the versions of uninstalled packages"
msgstr ""

#: src/cli.vala resources/preferences_dialog.ui
msgid "Clean cache"
msgstr ""

#: src/cli.vala resources/preferences_dialog.ui
msgid "Clean build files"
msgstr ""

#: src/cli.vala
#, c-format
msgid "There is %u member in group %s"
msgid_plural "There are %u members in group %s"
msgstr[0] ""
msgstr[1] ""

#: src/cli.vala
msgid "all"
msgstr ""

#: resources/choose_provider_dialog.ui
msgid "Choose a Provider"
msgstr ""

#: resources/choose_provider_dialog.ui resources/choose_pkgs_dialog.ui
msgid "Choose"
msgstr ""

#: resources/progress_dialog.ui
msgid "Progress"
msgstr "Progreso"

#: resources/history_dialog.ui
msgid "Pamac History"
msgstr ""

#: resources/transaction_sum_dialog.ui
msgid "Transaction Summary"
msgstr ""

#: resources/transaction_sum_dialog.ui resources/manager_window.ui
msgid "_Apply"
msgstr ""

#: resources/manager_window.ui
msgid "Refresh databases"
msgstr ""

#: resources/manager_window.ui
msgid "View History"
msgstr ""

#: resources/manager_window.ui resources/preferences_dialog.ui
msgid "Preferences"
msgstr "Preferoj"

#: resources/manager_window.ui
msgid "About"
msgstr ""

#: resources/manager_window.ui
msgid "Search"
msgstr "Ŝerĉi"

#: resources/manager_window.ui
msgid "All"
msgstr ""

#: resources/manager_window.ui
msgid "Pending"
msgstr ""

#: resources/manager_window.ui
msgid "Updates"
msgstr ""

#: resources/manager_window.ui
msgid "Remove all"
msgstr ""

#: resources/manager_window.ui
msgid "Install all"
msgstr ""

#: resources/manager_window.ui
msgid "Ignore all"
msgstr ""

#: resources/manager_window.ui
msgid "Sort by"
msgstr ""

#: resources/manager_window.ui
msgid "Relevance"
msgstr ""

#: resources/manager_window.ui
msgid "Date"
msgstr ""

#: resources/manager_window.ui
msgid "Browse"
msgstr ""

#: resources/manager_window.ui
msgid "Launch"
msgstr ""

#: resources/manager_window.ui
msgid "Reinstall"
msgstr "Reinstali"

#: resources/manager_window.ui
msgid "Build"
msgstr ""

#: resources/manager_window.ui
msgid "Reset build files"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Remove unrequired dependencies"
msgstr ""

#: resources/preferences_dialog.ui
msgid ""
"When removing a package, also remove its dependencies that are not required "
"by other packages"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Check available disk space"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Enable downgrade"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Check for updates"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Automatically download updates"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Hide tray icon when no update available"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Ignore upgrades for:"
msgstr ""

#: resources/preferences_dialog.ui
msgid "General"
msgstr "Ĝenerala"

#: resources/preferences_dialog.ui
msgid "Use mirrors from:"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Refresh Mirrors List"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Official Repositories"
msgstr ""

#: resources/preferences_dialog.ui
msgid ""
"AUR is a community maintained repository so it presents potential risks and problems.\n"
"All AUR users should be familiar with the build process."
msgstr ""

#: resources/preferences_dialog.ui
msgid "Enable AUR support"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Allow Pamac to search and install packages from AUR"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Check for updates from AUR"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Check for development packages updates"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Cache"
msgstr ""
