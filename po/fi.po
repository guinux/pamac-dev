# Translation of Pamac.
# Copyright (C) 2013-2019 Manjaro Developers <manjaro-dev@manjaro.org>
# This file is distributed under the same license as the Pamac package.
# Guillaume Benoit <guillaume@manjaro.org>, 2013-2019.
# 
# Translators:
# Ammuu5, 2017-2019
# Esa Päiviö, 2019
# Esa Päiviö, 2015
# EscoP, 2015
# Jari Lankinen <lankinen.jari@gmail.com>, 2018
# Luki <jbkammer@gmail.com>, 2013,2015
# Johannes Lakkisto <jclc@protonmail.com>, 2018
# Luki <jbkammer@gmail.com>, 2013,2015
# philm <philm@manjaro.org>, 2015
# Sami Korkalainen, 2013-2014
# Sami Korkalainen, 2014-2015
# SamiPerkele, 2013-2014
# Vesa Laitinen <vesa.laitinen@gmail.com>, 2017
# Sami Korkalainen, 2013
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pamac\n"
"Report-Msgid-Bugs-To: guillaume@manjaro.org\n"
"POT-Creation-Date: 2019-08-06 16:57+0200\n"
"PO-Revision-Date: 2019-08-07 00:47+0000\n"
"Last-Translator: philm <philm@manjaro.org>\n"
"Language-Team: Finnish (http://www.transifex.com/manjarolinux/manjaro-pamac/language/fi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/polkit/org.manjaro.pamac.policy.in
msgid "Authentication is required"
msgstr "Todennus vaaditaan"

#: src/alpm_utils.vala src/database.vala
msgid "Failed to initialize alpm library"
msgstr "alpm‐kirjaston valmistelu epäonnistui"

#: src/alpm_utils.vala
msgid "Failed to synchronize any databases"
msgstr "Tietokantojen synkronointi epäonnistui"

#: src/alpm_utils.vala
msgid "Failed to init transaction"
msgstr "Tapahtumaa ei saatu käynnistettyä"

#: src/alpm_utils.vala src/transaction.vala src/transaction-gtk.vala
msgid "Failed to prepare transaction"
msgstr "Tapahtumaa ei saatu valmisteltua"

#: src/alpm_utils.vala src/cli.vala
#, c-format
msgid "target not found: %s"
msgstr "kohdetta ei löydetty: %s"

#: src/alpm_utils.vala
#, c-format
msgid "package %s does not have a valid architecture"
msgstr "pakettilla %s ei ole kelvollista arkkitehtuuria"

#: src/alpm_utils.vala
#, c-format
msgid "unable to satisfy dependency '%s' required by %s"
msgstr "puuttuva riippuvuus '%s' paketilla %s"

#: src/alpm_utils.vala
#, c-format
msgid "installing %s (%s) breaks dependency '%s' required by %s"
msgstr "asentamalla %s (%s) rikkoo riippuvuuden '%s' jonka vaatii %s"

#: src/alpm_utils.vala
#, c-format
msgid "removing %s breaks dependency '%s' required by %s"
msgstr "paketin %s poistaminen rikkoo riippuvuuden '%s' jonka vaatii %s"

#: src/alpm_utils.vala
#, c-format
msgid "%s and %s are in conflict"
msgstr "%s ja %s ovat ristiriidassa"

#: src/alpm_utils.vala
#, c-format
msgid "%s needs to be removed but it is a locked package"
msgstr "%s täytyy poistaa mutta se on lukittu paketti"

#: src/alpm_utils.vala
msgid "Failed to commit transaction"
msgstr "Tapahtumaa ei saatu tehtyä."

#: src/alpm_utils.vala
#, c-format
msgid "%s exists in both %s and %s"
msgstr "%s löytyy paketeista %s ja %s"

#: src/alpm_utils.vala
#, c-format
msgid "%s: %s already exists in filesystem"
msgstr "%s: %s löytyy jo tiedostojärjestelmästä"

#: src/alpm_utils.vala
#, c-format
msgid "%s is invalid or corrupted"
msgstr "%s on viallinen tai vioittunut"

#: src/alpm_utils.vala
#, c-format
msgid "%s appears to be truncated: %jd/%jd bytes\n"
msgstr "%s näyttää olevan katkennut: %jd/%jd tavua\n"

#: src/alpm_utils.vala
#, c-format
msgid "failed retrieving file '%s' from %s : %s\n"
msgstr "tiedoston noutaminen epäonnistui '%s' palvelimelta %s : %s\n"

#: src/system_daemon.vala
msgid "Authentication failed"
msgstr "Todennus epäonnistui"

#: src/database.vala src/transaction-gtk.vala src/manager_window.vala
#: src/transaction-cli.vala src/cli.vala resources/preferences_dialog.ui
msgid "AUR"
msgstr "AUR"

#: src/database.vala src/manager_window.vala
msgid "Explicitly installed"
msgstr "Käyttäjän asentama"

#: src/database.vala src/manager_window.vala
msgid "Installed as a dependency for another package"
msgstr "Asennettu riippuvuutena toiselle paketille"

#: src/database.vala
msgid "Unknown"
msgstr "Tuntematon"

#: src/database.vala
msgid "Yes"
msgstr "Kyllä"

#: src/database.vala
msgid "No"
msgstr "Ei"

#: src/transaction.vala
msgid "Refreshing mirrors list"
msgstr "Päivitetään peilipalvelin lista"

#: src/transaction.vala
msgid "Preparing"
msgstr "Valmistellaan"

#: src/transaction.vala
msgid "Synchronizing package databases"
msgstr "Synkronoidaan pakettitietokantoja"

#: src/transaction.vala
msgid "Starting full system upgrade"
msgstr "Aloitetaan täysi järjestelmäpäivitys"

#: src/transaction.vala src/cli.vala
#, c-format
msgid "Cloning %s build files"
msgstr "Kloonataan %s koontitiedostoja"

#: src/transaction.vala
#, c-format
msgid "Generating %s informations"
msgstr "Luodaan %s tietoja"

#: src/transaction.vala
#, c-format
msgid "Checking %s dependencies"
msgstr "Tarkistetaan %s riippuvuuksia"

#: src/transaction.vala
#, c-format
msgid "key %s could not be imported"
msgstr "avainta %s ei voitu tuoda"

#: src/transaction.vala
msgid "Waiting for another package manager to quit"
msgstr "Odotetaan toisen pakettienhallintatyökalun lopettavan"

#: src/transaction.vala
#, c-format
msgid "Building %s"
msgstr "Kasataan %s"

#: src/transaction.vala
msgid "Transaction cancelled"
msgstr "Tapahtuma peruttiin"

#: src/transaction.vala
msgid "Checking dependencies"
msgstr "Selvitetään riippuvuuksia"

#: src/transaction.vala
msgid "Checking file conflicts"
msgstr "Tarkastetaan tiedostoristiriitoja"

#: src/transaction.vala
msgid "Resolving dependencies"
msgstr "Selvitetään riippuvuuksia"

#: src/transaction.vala
msgid "Checking inter-conflicts"
msgstr "Tarkistetaan pakettien välisiä ristiriitoja"

#: src/transaction.vala
#, c-format
msgid "Installing %s"
msgstr "Asennetaan %s"

#: src/transaction.vala
#, c-format
msgid "Upgrading %s"
msgstr "Päivitetään %s"

#: src/transaction.vala
#, c-format
msgid "Reinstalling %s"
msgstr "Uudelleenasennetaan %s"

#: src/transaction.vala
#, c-format
msgid "Downgrading %s"
msgstr "Varhennetaan %s"

#: src/transaction.vala
#, c-format
msgid "Removing %s"
msgstr "Poistetaan %s"

#: src/transaction.vala
msgid "Checking integrity"
msgstr "Tarkastetaan eheyttä"

#: src/transaction.vala
msgid "Loading packages files"
msgstr "Ladataan paketin tiedostoja"

#: src/transaction.vala
msgid "Checking delta integrity"
msgstr "Tarkistetaan delta-eheyttä"

#: src/transaction.vala
msgid "Applying deltas"
msgstr "Asetetaan deltoja"

#: src/transaction.vala
#, c-format
msgid "Generating %s with %s"
msgstr "Luodaan %s deltalla %s"

#: src/transaction.vala
msgid "Generation succeeded"
msgstr "Luominen onnistui"

#: src/transaction.vala
msgid "Generation failed"
msgstr "Luominen epäonnistui"

#: src/transaction.vala
#, c-format
msgid "Configuring %s"
msgstr "Konfiguroidaan %s"

#: src/transaction.vala
msgid "Checking available disk space"
msgstr "Tarkistetaan saatavilla olevaa levytilaa"

#: src/transaction.vala
#, c-format
msgid "%s optionally requires %s"
msgstr "%s vaatii valinnaisesti paketin %s"

#: src/transaction.vala
#, c-format
msgid "Database file for %s does not exist"
msgstr "Tietokantatiedostoa pakettivarastolle %s ei löydy"

#: src/transaction.vala
msgid "Checking keyring"
msgstr "Tarkastetaan avainrengasta"

#: src/transaction.vala
msgid "Downloading required keys"
msgstr "Ladataan vaadittuja avaimia"

#: src/transaction.vala
#, c-format
msgid "%s installed as %s.pacnew"
msgstr "%s asennettiin tiedostona %s.pacnew"

#: src/transaction.vala
#, c-format
msgid "%s installed as %s.pacsave"
msgstr "%s asennettiin tiedostona %s.pacsave"

#: src/transaction.vala
msgid "Running pre-transaction hooks"
msgstr "Suoritetaan asennusta edeltävät työt"

#: src/transaction.vala
msgid "Running post-transaction hooks"
msgstr "Suoritetaan asennuksen jälkeiset työt"

#: src/transaction.vala
#, c-format
msgid "Downloading %s"
msgstr "Ladataan %s"

#: src/transaction.vala
#, c-format
msgid "About %u seconds remaining"
msgstr "Noin %u sekunttia jäljellä"

#: src/transaction.vala
#, c-format
msgid "About %lu minute remaining"
msgid_plural "About %lu minutes remaining"
msgstr[0] "Noin %lu minuutti jäljellä"
msgstr[1] "Noin %lu minuuttia jäljellä"

#: src/transaction.vala
#, c-format
msgid "Refreshing %s"
msgstr "Virkistetään %s"

#: src/transaction.vala src/transaction-cli.vala src/cli.vala
msgid "Error"
msgstr "Virhe"

#: src/transaction.vala src/transaction-gtk.vala
msgid "Warning"
msgstr "Varoitus"

#: src/transaction.vala src/installer.vala src/cli.vala
msgid "Nothing to do"
msgstr "Ei mitään tehtävää"

#: src/transaction.vala src/transaction-gtk.vala
msgid "Transaction successfully finished"
msgstr "Toimenpide suoritettu onnistuneesti"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Choose optional dependencies for %s"
msgstr "Valitse valinnaiset riippuvuudet paketille %s"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Choose a provider for %s"
msgstr "Valitse tarjoaja kohteelle %s"

#: src/transaction-gtk.vala
msgid "Import PGP key"
msgstr "Tuo PGP-avain"

#: src/transaction-gtk.vala
msgid "Trust and Import"
msgstr ""

#: src/transaction-gtk.vala src/manager_window.vala
#: resources/transaction_sum_dialog.ui resources/manager_window.ui
#: resources/choose_pkgs_dialog.ui
msgid "_Cancel"
msgstr "_Peruuta"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "The PGP key %s is needed to verify %s source files"
msgstr "PGP-avain %s vaaditaan lähdetiedostojen %s todentamiseksi"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Trust %s and import the PGP key"
msgstr "Merkitse luotetuksi, ja tuo PGP-avain %s"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To remove"
msgstr "Poistettavat"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To downgrade"
msgstr "Varhennettavat"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To build"
msgstr "Käännettävät"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To install"
msgstr "Asennettavat"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To reinstall"
msgstr "Uudelleenasennettavat"

#: src/transaction-gtk.vala src/transaction-cli.vala
msgid "To upgrade"
msgstr "Päivitettävät"

#: src/transaction-gtk.vala src/manager_window.vala src/transaction-cli.vala
msgid "Total download size"
msgstr "Latauksen kokonaiskoko"

#: src/transaction-gtk.vala src/transaction-cli.vala
#, c-format
msgid "Edit %s build files"
msgstr "Muokkaa %skoontitiedostoja"

#: src/transaction-gtk.vala
msgid "Save"
msgstr "Tallenna"

#: src/transaction-gtk.vala resources/progress_dialog.ui
#: resources/history_dialog.ui resources/preferences_dialog.ui
msgid "_Close"
msgstr "_Sulje"

#: src/transaction-gtk.vala src/tray.vala
msgid "Package Manager"
msgstr "Paketinhallinta"

#: src/installer.vala src/cli.vala
msgid "Install packages from repositories, path or url"
msgstr "Asenna paketteja pakettivarastoista, polusta tai URL:ista"

#: src/installer.vala src/cli.vala
msgid "Remove packages"
msgstr "Poista paketteja"

#: src/installer.vala src/cli.vala
msgid "package(s)"
msgstr "paketti"

#: src/tray.vala src/manager_window.vala src/cli.vala
msgid "Your system is up-to-date"
msgstr "Järjestelmäsi on ajan tasalla"

#: src/tray.vala
msgid "_Quit"
msgstr "_Poistu"

#: src/tray.vala src/cli.vala
#, c-format
msgid "%u available update"
msgid_plural "%u available updates"
msgstr[0] "%u saatavilla oleva päivitys"
msgstr[1] "%u saatavilla olevaa päivitystä"

#: src/tray.vala src/manager_window.vala resources/progress_dialog.ui
#: resources/manager_window.ui
msgid "Details"
msgstr "Yksityiskohdat"

#: src/manager_window.vala
msgid "No package found"
msgstr "Pakettia ei löytynyt"

#: src/manager_window.vala
msgid "Checking for Updates"
msgstr "Päivitetään pakettilista"

#: src/manager_window.vala
#, c-format
msgid "%u pending operation"
msgid_plural "%u pending operations"
msgstr[0] "%u operaatio odottaa"
msgstr[1] "%u operaatiota odottaa"

#: src/manager_window.vala
msgid "Categories"
msgstr "Kategoriat"

#: src/manager_window.vala src/cli.vala
msgid "Groups"
msgstr "Ryhmät"

#: src/manager_window.vala
msgid "Repositories"
msgstr "Pakettivarastot"

#: src/manager_window.vala src/cli.vala resources/manager_window.ui
msgid "Installed"
msgstr "Asennettuna"

#: src/manager_window.vala
msgid "Orphans"
msgstr "Orvot"

#: src/manager_window.vala
msgid "Foreign"
msgstr "Vieras"

#: src/manager_window.vala
msgid "Accessories"
msgstr "Apuohjelmat"

#: src/manager_window.vala
msgid "Audio & Video"
msgstr "Ääni ja video"

#: src/manager_window.vala
msgid "Development"
msgstr "Kehitys"

#: src/manager_window.vala
msgid "Education"
msgstr "Opetus"

#: src/manager_window.vala
msgid "Games"
msgstr "Pelit"

#: src/manager_window.vala
msgid "Graphics"
msgstr "Grafiikka"

#: src/manager_window.vala
msgid "Internet"
msgstr "Internet"

#: src/manager_window.vala
msgid "Office"
msgstr "Toimisto"

#: src/manager_window.vala
msgid "Science"
msgstr "Tiede"

#: src/manager_window.vala
msgid "Settings"
msgstr "Asetukset"

#: src/manager_window.vala
msgid "System Tools"
msgstr "Järjestelmän hallinta"

#: src/manager_window.vala
msgid "Dependencies"
msgstr "Riippuvuudet"

#: src/manager_window.vala
msgid "Files"
msgstr "Tiedostot"

#: src/manager_window.vala
msgid "Build files"
msgstr "Koontitiedostot"

#: src/manager_window.vala src/cli.vala
msgid "Install Reason"
msgstr "Asennuksen syy"

#: src/manager_window.vala
msgid "Mark as explicitly installed"
msgstr "Merkitse erikseen asennetuksi"

#: src/manager_window.vala resources/manager_window.ui
msgid "Install"
msgstr "Asenna"

#: src/manager_window.vala src/cli.vala
msgid "Licenses"
msgstr "Lisenssit"

#: src/manager_window.vala src/cli.vala resources/manager_window.ui
msgid "Repository"
msgstr "Pakettivarasto"

#: src/manager_window.vala src/cli.vala
msgid "Package Base"
msgstr "Perus paketti"

#: src/manager_window.vala src/cli.vala
msgid "Maintainer"
msgstr "Ylläpitäjä"

#: src/manager_window.vala src/cli.vala
msgid "First Submitted"
msgstr "Ensimmäinen versio"

#: src/manager_window.vala src/cli.vala
msgid "Last Modified"
msgstr "Viimeksi muutettu"

#: src/manager_window.vala src/cli.vala
msgid "Votes"
msgstr "Ääniä"

#: src/manager_window.vala src/cli.vala
msgid "Out of Date"
msgstr "Vanhentunut"

#: src/manager_window.vala src/cli.vala
msgid "Packager"
msgstr "Pakkaaja"

#: src/manager_window.vala src/cli.vala
msgid "Build Date"
msgstr "Koontipäivämäärä"

#: src/manager_window.vala src/cli.vala
msgid "Install Date"
msgstr "Asennuspäivämäärä"

#: src/manager_window.vala src/cli.vala
msgid "Signatures"
msgstr "Allekirjoitukset"

#: src/manager_window.vala src/cli.vala
msgid "Backup files"
msgstr "Varmuuskopiot"

#: src/manager_window.vala src/cli.vala
msgid "Depends On"
msgstr "Riippuu paketeista"

#: src/manager_window.vala src/cli.vala
msgid "Optional Dependencies"
msgstr "Valinnaiset riippuvuudet"

#: src/manager_window.vala src/cli.vala
msgid "Required By"
msgstr "Riippuvuutena paketeille"

#: src/manager_window.vala src/cli.vala
msgid "Optional For"
msgstr "Valinnainen riippuvuus"

#: src/manager_window.vala src/cli.vala
msgid "Provides"
msgstr "Tarjoaa"

#: src/manager_window.vala src/cli.vala
msgid "Replaces"
msgstr "Korvaa"

#: src/manager_window.vala src/cli.vala
msgid "Conflicts With"
msgstr "Ristiriidat"

#: src/manager_window.vala src/cli.vala
msgid "Make Dependencies"
msgstr "Kasaa riippuvuudet"

#: src/manager_window.vala src/cli.vala
msgid "Check Dependencies"
msgstr "Tarkista riippuvuudet"

#: src/manager_window.vala
msgid "Upgrade"
msgstr "Päivitä"

#: src/manager_window.vala resources/manager_window.ui
msgid "Remove"
msgstr "Poista"

#: src/manager_window.vala src/cli.vala resources/manager_window.ui
msgid "Size"
msgstr "Koko"

#: src/manager_window.vala resources/manager_window.ui
msgid "Install Local Packages"
msgstr "Asenna paikalliset paketit"

#: src/manager_window.vala
msgid "_Open"
msgstr "_Avaa"

#: src/manager_window.vala
msgid "Alpm Package"
msgstr "Alpm paketti"

#: src/manager_window.vala
msgid "A Gtk3 frontend for libalpm"
msgstr "Gtk3‐edustaohjelma libalpm:lle"

#: src/preferences_dialog.vala resources/preferences_dialog.ui
msgid "How often to check for updates, value in hours"
msgstr "Kuinka usein tarkistetaan päivityksiä, arvo tunneissa"

#: src/preferences_dialog.vala resources/preferences_dialog.ui
msgid "Maximum parallel downloads"
msgstr "Yhtäaikaisten latausten enimmäismäärä"

#: src/preferences_dialog.vala src/cli.vala resources/preferences_dialog.ui
msgid "Number of versions of each package to keep in the cache"
msgstr "Paketin versioiden määrä joka säilytetään välimuistissa"

#: src/preferences_dialog.vala
msgid "Build directory"
msgstr "Koontihakemisto"

#: src/preferences_dialog.vala
msgid "Worldwide"
msgstr "Maailmanlaajuinen"

#: src/preferences_dialog.vala src/cli.vala
msgid "To delete"
msgstr "Poistettavana"

#: src/preferences_dialog.vala src/cli.vala
#, c-format
msgid "%u file"
msgid_plural "%u files"
msgstr[0] "%u tiedosto"
msgstr[1] "%u tiedostoa"

#: src/preferences_dialog.vala
msgid "Choose Ignored Upgrades"
msgstr "Valitse ohitettavat päivitykset"

#: src/transaction-cli.vala src/cli.vala
#, c-format
msgid "Enter a selection (default=%s)"
msgstr "Syötä valinta (oletus=%s)"

#: src/transaction-cli.vala
msgid "none"
msgstr "ei mikään"

#: src/transaction-cli.vala
#, c-format
msgid "Enter a number (default=%d)"
msgstr "Syötä numero (oletus=%d)"

#: src/transaction-cli.vala src/cli.vala
msgid "[y/N]"
msgstr "[k/N]"

#: src/transaction-cli.vala src/cli.vala
msgid "y"
msgstr "k"

#: src/transaction-cli.vala src/cli.vala
msgid "yes"
msgstr "kyllä"

#: src/transaction-cli.vala resources/transaction_sum_dialog.ui
msgid "Edit build files"
msgstr "Muokkaa koontitiedostoja"

#: src/transaction-cli.vala
msgid "Total installed size"
msgstr "Koko asennetuna yhteensä"

#: src/transaction-cli.vala
msgid "Total removed size"
msgstr "Koko poistettuna yhteensä"

#: src/transaction-cli.vala
msgid "Apply transaction"
msgstr ""

#: src/transaction-cli.vala
#, c-format
msgid "View %s build files diff"
msgstr "Näytä %s koontitiedostojen eroavaisuudet"

#: src/cli.vala
msgid "Building packages as root is not allowed"
msgstr "Pakettien koontia root-käyttäjänä ei ole sallittu"

#: src/cli.vala
msgid "No PKGBUILD file found in current directory"
msgstr "PKGBUILD-tiedostoa ei löytynyt nykyisestä hakemistosta"

#: src/cli.vala
msgid "Check development packages updates as root is not allowed"
msgstr "Kehityspakettien päivitysten tarkistaminen root-käyttäjänä ei ole sallittu"

#: src/cli.vala
msgid "Available actions"
msgstr "Saatavilla olevat toiminnot"

#: src/cli.vala
msgid "action"
msgstr "toiminto"

#: src/cli.vala
msgid "options"
msgstr "asetukset"

#: src/cli.vala
msgid "Search for packages or files, multiple search terms can be specified"
msgstr "Etsi paketteja tai tiedostoja, voit määritellä useampia hakutermejä"

#: src/cli.vala
msgid "file(s)"
msgstr "tiedosto(t)"

#: src/cli.vala
msgid "also search in AUR"
msgstr "etsi myös AUR:ista"

#: src/cli.vala
msgid ""
"search for packages which own the given filenames (filenames can be partial)"
msgstr ""

#: src/cli.vala
msgid "Display package details, multiple packages can be specified"
msgstr ""

#: src/cli.vala
msgid "List packages, groups, repositories or files"
msgstr ""

#: src/cli.vala
msgid "group(s)"
msgstr "ryhmä(t)"

#: src/cli.vala
msgid "repo(s)"
msgstr "pakettivarasto(t)"

#: src/cli.vala
msgid "list installed packages"
msgstr "Listaa asennetut paketit"

#: src/cli.vala
msgid ""
"list packages that were installed as dependencies but are no longer required"
" by any installed package"
msgstr ""

#: src/cli.vala
msgid "list packages that were not found in the repositories"
msgstr ""

#: src/cli.vala
msgid ""
"list all packages that are members of the given groups, if no group is given"
" list all groups"
msgstr ""

#: src/cli.vala
msgid ""
"list all packages available in the given repos, if no repo is given list all"
" repos"
msgstr ""

#: src/cli.vala
msgid "list files owned by the given packages"
msgstr ""

#: src/cli.vala
msgid "Clone or sync packages build files from AUR"
msgstr ""

#: src/cli.vala
msgid "dir"
msgstr ""

#: src/cli.vala
msgid ""
"build directory, if no directory is given the one specified in pamac.conf "
"file is used"
msgstr ""

#: src/cli.vala
msgid "also clone needed dependencies"
msgstr ""

#: src/cli.vala
msgid "overwrite existing files"
msgstr ""

#: src/cli.vala
msgid "Build packages from AUR and install them with their dependencies"
msgstr ""

#: src/cli.vala
msgid ""
"If no package name is given, use the PKGBUILD file in the current directory"
msgstr ""

#: src/cli.vala
msgid ""
"The build directory will be the parent directory, --builddir option will be "
"ignored"
msgstr ""

#: src/cli.vala
msgid "do not clone build files from AUR, only use local files"
msgstr "älä kloonaa koontitiedostoja AUR:ista, käytä vain paikallisia tiedostoja"

#: src/cli.vala
msgid "bypass any and all confirmation messages"
msgstr ""

#: src/cli.vala
msgid "glob"
msgstr ""

#: src/cli.vala
msgid ""
"ignore a package upgrade, multiple packages can be specified by separating "
"them with a comma"
msgstr ""

#: src/cli.vala
msgid ""
"overwrite conflicting files, multiple patterns can be specified by "
"separating them with a comma"
msgstr ""

#: src/cli.vala
msgid "Reinstall packages"
msgstr "Asenna paketit uudelleen"

#: src/cli.vala
msgid ""
"remove dependencies that are not required by other packages, if this option "
"is used without package name remove all orphans"
msgstr ""

#: src/cli.vala
msgid "Safely check for updates without modifiying the databases"
msgstr ""

#: src/cli.vala
msgid "Exit code is 100 if updates are available"
msgstr ""

#: src/cli.vala
msgid "also check updates in AUR"
msgstr "tarkista myös päivitykset AUR:ista"

#: src/cli.vala
msgid "also check development packages updates (use with --aur)"
msgstr "tarkista myös kehityspakettien päivitykset (käytä --aur)"

#: src/cli.vala
msgid "only print one line per update"
msgstr "tulosta vain yksi rivi päivitystä kohden"

#: src/cli.vala
msgid ""
"build directory (use with --devel), if no directory is given the one "
"specified in pamac.conf file is used"
msgstr ""

#: src/cli.vala
msgid "Upgrade your system"
msgstr "Päivitä järjestelmäsi"

#: src/cli.vala
msgid "also upgrade packages installed from AUR"
msgstr "päivitä myös AUR:ista asennetut paketit"

#: src/cli.vala
msgid "also upgrade development packages (use with --aur)"
msgstr ""

#: src/cli.vala
msgid ""
"build directory (use with --aur), if no directory is given the one specified"
" in pamac.conf file is used"
msgstr ""

#: src/cli.vala
msgid "force the refresh of the databases"
msgstr "pakota tietokantojen päivitys"

#: src/cli.vala
msgid "enable package downgrades"
msgstr "ota pakettien vanhentaminen käyttöön"

#: src/cli.vala
msgid "Clean packages cache or build files"
msgstr "Tyhjennä pakettien välimuisti ja koontitiedostot"

#: src/cli.vala
msgid "number"
msgstr "numero"

#: src/cli.vala
msgid ""
"specify how many versions of each package are kept in the cache directory"
msgstr "määritä, kuinka monta versiota kustakin paketista säilytetään välimuistihakemistossa"

#: src/cli.vala
msgid "only target uninstalled packages"
msgstr ""

#: src/cli.vala
msgid ""
"remove all build files, the build directory is the one specified in "
"pamac.conf"
msgstr ""

#: src/cli.vala
msgid "do not remove files, only find candidate packages"
msgstr ""

#: src/cli.vala
msgid "also display all files names"
msgstr ""

#: src/cli.vala resources/manager_window.ui
msgid "Name"
msgstr "Nimi"

#: src/cli.vala
msgid "Version"
msgstr "Versio"

#: src/cli.vala
msgid "Description"
msgstr "Kuvaus"

#: src/cli.vala
msgid "URL"
msgstr "URL"

#: src/cli.vala
#, c-format
msgid "No package owns %s"
msgstr ""

#: src/cli.vala
#, c-format
msgid "%s is owned by %s"
msgstr ""

#: src/cli.vala resources/preferences_dialog.ui
msgid "Remove only the versions of uninstalled packages"
msgstr "Poista vain asentamattomien pakettien versiot"

#: src/cli.vala resources/preferences_dialog.ui
msgid "Clean cache"
msgstr "Tyhjennä välimuisti"

#: src/cli.vala resources/preferences_dialog.ui
msgid "Clean build files"
msgstr "Tyhjennä koontitiedostot"

#: src/cli.vala
#, c-format
msgid "There is %u member in group %s"
msgid_plural "There are %u members in group %s"
msgstr[0] "Ryhmässä %s on %u jäsen"
msgstr[1] "Ryhmässä %s on %u jäsentä"

#: src/cli.vala
msgid "all"
msgstr "kaikki"

#: resources/choose_provider_dialog.ui
msgid "Choose a Provider"
msgstr "Valitse toimittaja"

#: resources/choose_provider_dialog.ui resources/choose_pkgs_dialog.ui
msgid "Choose"
msgstr "Valitse"

#: resources/progress_dialog.ui
msgid "Progress"
msgstr "Edistyminen"

#: resources/history_dialog.ui
msgid "Pamac History"
msgstr "Pamacin historia"

#: resources/transaction_sum_dialog.ui
msgid "Transaction Summary"
msgstr "Toimenpiteiden yhteenveto"

#: resources/transaction_sum_dialog.ui resources/manager_window.ui
msgid "_Apply"
msgstr "_hyväksy"

#: resources/manager_window.ui
msgid "Refresh databases"
msgstr "Päivitä tietokannat"

#: resources/manager_window.ui
msgid "View History"
msgstr "Näytä historia"

#: resources/manager_window.ui resources/preferences_dialog.ui
msgid "Preferences"
msgstr "Asetukset"

#: resources/manager_window.ui
msgid "About"
msgstr "Tietoja"

#: resources/manager_window.ui
msgid "Search"
msgstr "Etsi"

#: resources/manager_window.ui
msgid "All"
msgstr ""

#: resources/manager_window.ui
msgid "Pending"
msgstr "Odottaa"

#: resources/manager_window.ui
msgid "Updates"
msgstr "Päivitykset"

#: resources/manager_window.ui
msgid "Remove all"
msgstr ""

#: resources/manager_window.ui
msgid "Install all"
msgstr ""

#: resources/manager_window.ui
msgid "Ignore all"
msgstr ""

#: resources/manager_window.ui
msgid "Sort by"
msgstr ""

#: resources/manager_window.ui
msgid "Relevance"
msgstr ""

#: resources/manager_window.ui
msgid "Date"
msgstr ""

#: resources/manager_window.ui
msgid "Browse"
msgstr "Selaa"

#: resources/manager_window.ui
msgid "Launch"
msgstr ""

#: resources/manager_window.ui
msgid "Reinstall"
msgstr "Asenna uudelleen"

#: resources/manager_window.ui
msgid "Build"
msgstr "Koonti"

#: resources/manager_window.ui
msgid "Reset build files"
msgstr ""

#: resources/preferences_dialog.ui
msgid "Remove unrequired dependencies"
msgstr "Poista ei-vaaditut riippuvuudet"

#: resources/preferences_dialog.ui
msgid ""
"When removing a package, also remove its dependencies that are not required "
"by other packages"
msgstr "Pakettia poistettaessa, poista myös pakettiriippuvuudet jotka eivät ole muiden pakettien käytössä"

#: resources/preferences_dialog.ui
msgid "Check available disk space"
msgstr "Tarkista kovalevyn käytettävissä oleva koko"

#: resources/preferences_dialog.ui
msgid "Enable downgrade"
msgstr "Ota vanhentaminen käyttöön"

#: resources/preferences_dialog.ui
msgid "Check for updates"
msgstr "Tarkista päivitykset"

#: resources/preferences_dialog.ui
msgid "Automatically download updates"
msgstr "Lataa päivitykset automaattisesti"

#: resources/preferences_dialog.ui
msgid "Hide tray icon when no update available"
msgstr "Piilota pikku kuvake kun ei ole päivitettävää"

#: resources/preferences_dialog.ui
msgid "Ignore upgrades for:"
msgstr "Jätä huomitta nämä paketit:"

#: resources/preferences_dialog.ui
msgid "General"
msgstr "Yleiset"

#: resources/preferences_dialog.ui
msgid "Use mirrors from:"
msgstr "Käytä peilipalvelimia sijainnista:"

#: resources/preferences_dialog.ui
msgid "Refresh Mirrors List"
msgstr "Päivitä peilipalvelin lista"

#: resources/preferences_dialog.ui
msgid "Official Repositories"
msgstr "Viralliset pakettilähteet"

#: resources/preferences_dialog.ui
msgid ""
"AUR is a community maintained repository so it presents potential risks and problems.\n"
"All AUR users should be familiar with the build process."
msgstr "AUR on yhteisön ylläpitämä pakettilähde, joten se voi tuoda tietoturvariskejä ja ongelmia.\nKaikkien AUR-käyttäjän pitäisi tuntea pakettien teko prosessi."

#: resources/preferences_dialog.ui
msgid "Enable AUR support"
msgstr "Ota AUR-tuki käyttöön"

#: resources/preferences_dialog.ui
msgid "Allow Pamac to search and install packages from AUR"
msgstr "Anna Pamacin etsiä ja asentaa paketteja AUR:sta"

#: resources/preferences_dialog.ui
msgid "Check for updates from AUR"
msgstr "Tarkista päivitykset AUR pakettilähteestä"

#: resources/preferences_dialog.ui
msgid "Check for development packages updates"
msgstr "Tarkista kehityspakettien päivitykset"

#: resources/preferences_dialog.ui
msgid "Cache"
msgstr "Välimuisti"
